class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  # :validatable :recoverable :rememberable
  devise  :database_authenticatable,
          :registerable,
          :trackable,
          :omniauthable,
          omniauth_providers: [:twitter]

  has_one :job_profile
  has_many :identities

  validates_presence_of :email

  def self.create_with_omniauth(auth)
    create do |user|
      user.email = auth['email']
      user.password = Devise.friendly_token[0,20]
      user.name = auth['name']   # assuming the user model has a name
      #user.image = auth.info.image # assuming the user model has an image
    end
  end


  def self.new_with_session(params, session)
    if session['devise.user_attributes'] and session['devise.identity_attributes'] 
      new(session['devise.user_attributes'], without_protection: true) do |user|
        user.attributes = params
        user.valid?
      end
    else
      super
    end
  end
end