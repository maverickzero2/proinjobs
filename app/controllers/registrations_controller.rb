class RegistrationsController < Devise::RegistrationsController
  def create
    super do
      if resource.persisted?
        auth = session['devise.identity_attributes']
        identity = Identity.find_or_create_by(auth)
        identity.user = resource
        identity.save
      end
    end
  end
end